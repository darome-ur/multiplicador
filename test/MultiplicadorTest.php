<?php
use app\Multiplicador;
use PHPUnit\Framework\TestCase;
class MultiplicadorTest extends TestCase{
    /**  @test **/
    public function testMult(): void{
        $c = new Multiplicador(6);
        $this->assertEquals(36,$c->devolver());

        $c = new Multiplicador(5);
        $this->assertEquals(30,$c->devolver()); 
    }
}
?>
