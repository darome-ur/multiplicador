<?php
namespace app;
class Multiplicador{

    private $num;
    
    public function __construct($n){
        $this -> num = $n*6;
    }
    public function devolver(){
        $valor = $this -> num;
        return (int) $valor;
    }
    public function mostrar(): void{
        print "El numero es : $this -> num";
    }
}
?>